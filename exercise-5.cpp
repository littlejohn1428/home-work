#include <iostream>
using namespace std;

struct Student
{
    string FirstName;
    string LastName;
    string StudentNumber;
    int Credits;
};


Student CreateStudent() {
    Student s;

    cout << "Enter first name: ";
    cin >> s.FirstName;
    cout << "Enter last name: ";
    cin >> s.LastName;
    cout << "Enter student number: ";
    cin >> s.StudentNumber;
    cout << "Enter student's credits: ";
    cin >> s.Credits;

    return s;
}

int main() {
    
    Student Student1 = CreateStudent();

    cout << "Student's info:" << endl;

    cout << "First Name: " << Student1.FirstName << endl;
    cout << "Last Name: " << Student1.LastName << endl;
    cout << "Student Number: " << Student1.StudentNumber << endl;
    cout << "Student Credits: " << Student1.Credits << endl;

    return 0;
}