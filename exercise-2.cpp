#include <iostream>
#include <string>
using namespace std;

struct State
{
    string name;
    int population;
    int area;
    string leader;
    
};


int main() {
    State s1, s2;

    cout << "Insert the name of the 1. state: ";
    cin.ignore();
    getline(cin, s1.name);
    cout << "Insert the population: ";
    cin >> s1.population;
    cout << "Insert the area: ";
    cin >> s1.area;
    cout << "Insert the leader's name: ";
    cin.ignore();
    getline(cin, s1.leader);

    cout << "Insert the name of the 2. state: ";
    cin.ignore();
    getline(cin, s2.name);
    cout << "Insert the population: ";
    cin >> s2.population;
    cout << "Insert the area: ";
    cin >> s2.area;
    cout << "Insert the leader's name: ";
    cin.ignore();
    getline(cin, s2.leader);
    
    cout << "State 1 Information" << endl;
    cout << "State name: " << s1.name << endl;
    cout << "Population: " << s1.population << endl;
    cout << "Area: " << s1.area << endl;
    cout << "Leader name: " << s1.leader << endl << endl;

    cout << "State 2 Information" << endl;
    cout << "State name: " << s2.name << endl;
    cout << "Population: " << s2.population << endl;
    cout << "Area: " << s2.area << endl;
    cout << "Leader name: " << s2.leader << endl;

    return 0;
}