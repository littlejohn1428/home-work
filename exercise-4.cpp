#include <iostream>
using namespace std;

struct Student
{
    string FirstName;
    string LastName;
    string StudentNumber;
    int Credits;
};


void printStudentInformation(Student s) {
    cout << "Student's info:" << endl;

    cout << "First Name: " << s.FirstName << endl;
    cout << "Last Name: " << s.LastName << endl;
    cout << "Student Number: " << s.StudentNumber << endl;
    cout << "Student Credits: " << s.Credits << endl;
}

int main() {
    
    Student s;

    cout << "Enter first name: ";
    cin >> s.FirstName;
    cout << "Enter last name: ";
    cin >> s.LastName;
    cout << "Enter student number: ";
    cin >> s.StudentNumber;
    cout << "Enter student's credits: ";
    cin >> s.Credits;

    printStudentInformation(s);

    return 0;
}