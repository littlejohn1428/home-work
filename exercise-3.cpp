#include <iostream>
#include <string>
using namespace std;

struct State
{
    string name;
    int population;
    int area;
    string leader;
    
};


int main() {
    State array[5];

    for(int i=0; i<5; i++) {
        cout << "Insert the name of the " << (i+1) << ". state: ";
        cin.ignore();
        getline(cin, array[i].name);
        cout << "Insert the population: ";
        cin >> array[i].population;
        cout << "Insert the area: ";
        cin >> array[i].area;
        cout << "Insert the leader's name: ";
        cin.ignore();
        getline(cin, array[i].leader);
    }
    
    for(int i=0; i<5; i++) {
        cout << "State " << (i+1) << " Information" << endl;
        cout << "State name: " << array[i].name << endl;
        cout << "Population: " << array[i].population << endl;
        cout << "Area: " << array[i].area << endl;
        cout << "Leader name: " << array[i].leader << endl << endl;
    }
    
    return 0;
}